<?php http_response_code(404); ?>

<!doctype html>

<html lang="de">
    <head>
        <title>404 - Seite nicht gefunden</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </head>
    <body style="text-align: center;">
        <h1>Fehler: 404</h1>
        <h2>Diese Seite wurde nicht gefunden!</h2>
        <p>Die von Ihnen aufgerufene Seite wurde leider nicht gefunden.</p>
        <a href="/">Hier geht es zur Startseite</a>
    </body>
</html>