<?php $age = date_diff(date_create('1997-09-01'), date_create(date()))->format('%y'); ?>

<!doctype html>

<html lang="de">
    <head>
        <title>Webseite von Robin Gillitzer</title>
        
        <meta charset="utf-8">
        <meta name="description" content="Auf dieser Seite erhalten Sie Kontaktinformationen von Robin Gillitzer, 46325 Borken.">
        <meta name="author" content="Robin Gillitzer">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;400;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="css/styles.css">

        <link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">

        <script type="application/ld+json">
        {
        "@context": "https://schema.org",
        "@type": "Person",
        "address": {
            "@type": "PostalAddress",
            "addressLocality": "Borken",
            "postalCode": "46325"
        },
        "email": "schema@robin-gillitzer.de",
        "image": "img/avatar.png",
        "jobTitle": "Software Developer",
        "name": "Robin Gillitzer",
        "gender": "male",
        "nationality": "German",
        "url": "https://robin-gillitzer.de"
        }
        </script>
    </head>
    <body>

        <section class="d-flex h-100 align-items-center">

            <div class="container">
                <div class="contact-card">
                    <div class="row">
                        <article class="col-12 col-sm-8">
                            <div class="row">
                                <div class="col-12">
                                    <h1>Robin Gillitzer</h1>
                                    <h2>Software Developer &amp; Designer</h2>
                                    <p><?= $age; ?> Jahre alt, aus 46325 Borken (Deutschland) und arbeitet bei Mergelsberg Media in Borken.</p>
                                    <a class="btn btn-custom contact">kontakt aufnehmen <i class="far fa-envelope-open ml-2"></i></a>
                                </div>
                                <div class="col-12 mt-4">
                                    <ul class="icon-group">
                                        <li><a href="https://www.xing.com/profile/Robin_Gillitzer2"><img src="img/xing-icon.svg" alt="Xing" width="32"></a></li>
                                        <li><a href="https://stackoverflow.com/users/12872923/robin-gillitzer"><img src="img/stackoverflow-icon.svg"  alt="Stackoverflow" width="32"></a></li>
                                    </ul>
                                </div>
                            </div>

                        </article>
                        <div class="col-12 col-sm-4">
                           <img class="avatar" src="img/avatar.png" alt="Robin Gillitzer">
                        </div>
                    </div>
                </div>
            </div>

        </section>

        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
        <script src="https://kit.fontawesome.com/a01638c030.js" crossorigin="anonymous"></script>
        <script src="js/app.js"></script>
    </body>
</html>
